This asp.net core web api project
developed in Visual Studio Code ide 
(.Net core 2.2.401, AspNetCore 2.2.6)
Use DotnetCli to Run and Test Project (dotnet run and
dotnet test commands). Additional commands:
dotnet build; dotnet clean.
Run "dotnet test" in Tests project subfolder.
To run properly these commands, 
make sure the obj folder is deleted in Tests subfolder
(Or delete Tests.Program.cs file in BooksApi\Tests\obj\Debug
\netcoreapp2.2).
Testing framework - Nunit, Moq.
Swagger configured.

Model Book class has several constructor,
including default one to automatically convert
json to objects, copy constructor to generate
automatic static id, if absent,
and 2-and 3-parameters constructors (with and without
id parameter) to use in post requests.
Put requests use just the second one.
All repository "crud" methods use
book(list of books) return.
Settleid method allow to generate
unique id, that emulate database id
generation.

