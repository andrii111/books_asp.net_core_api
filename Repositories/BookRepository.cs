using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

using BooksApi.Models;
using BooksApi.Repositories;

namespace BooksApi.Repositories
{
    public class BookRepository : IRepository<Book>  {

        //internal repository book list
    static IList<Book> books=new List<Book>();

    public int Count()
    {
        return books.Count;
    }

        public IList<Book> GetBooks()
        {
            return books;
        }

        public Book GetBook(int id)
        {
            Book book=null;
            foreach (Book b in books) {
                if (b.Id == id) { book = b; break; }
            }
            return book;
        }

        public Book UpdateBook(Book item)
        {
            Book book = null;
            foreach (Book b in books)
            {
                if (b.Id == item.Id)
                {
                    b.Title = item.Title;
                    b.Author = item.Author; 
                    book = item; break;
                }
            }
            return book;
        }

        public Book AddBook(Book item)
        {
            Book book = new Book(item);

            SettleId(books,book);

            books.Add(book);
            return item;
        }

        public Book DeleteBook(int id)
        {
            Book book = null;
            for (int i = books.Count - 1; i >= 0; i--)
            {
                if (books[i].Id == id) {
                book = books[i];
                books.RemoveAt(i); break;
                 }
            }
            return book;
        }

        public void SettleId(IList<Book> books, Book book)
        {
            foreach (Book b in books)
            {
                if (b.Id == book.Id)
                {
                    int id = book.Id;
                    Boolean f = false;
                    while (!f)
                    {
                        int id2 = id;
                        foreach (Book b0 in books)
                        { if (b0.Id == id) { id++; break; } }
                        if (id == id2) { f = true; }
                    }
                    book.Id = id;
                }
            }
        }

        public void ClearRepo()
        {
            books.Clear();
        }

    }

}